# -*- coding: utf8 -*-
# python 3
# (C) Fabrice Sincère

from digital_filter_tools import *
from digital_filter_tools import _sup  # affichage sous forme d'exposant
from digital_filter_tools import _arrondi

print('''
Exemple d'utilisation du module digital_filter_tools
''')

__version__ = (0, 4, 1)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"


print("""Transmittance en z avec pôles et zéros :

          (z-z1).(z-z2)...(z-zm)
H(z) = k. _____________________________
          (z-p1).(z-p2).(z-p3)...(z-pn)

* n >= m
* pôles et zéros réels ou complexes par paires conjuguées
""")

'''
exemples
lowpass fir moving average
k = 0.125
(0.7071067811865476-0.7071067811865475j),(6.123233995736766e-17-1j),(-0.7071067811865475-0.7071067811865476j),-1.0,(-0.7071067811865477+0.7071067811865475j),(-1.8369701987210297e-16+1j),(0.7071067811865474+0.7071067811865477j)

0.0,0.0,0.0,0.0,0.0,0.0,0.0

lowpass iir moving average
k = 0.125

-1.0000000000000007,(-0.7071067811865491+0.707106781186549j),(-0.7071067811865482-0.7071067811865485j),(3.277054556403377e-16+1.0000000000000004j),(2.8517974007313276e-17-0.9999999999999999j),(0.7071067811865478+0.7071067811865478j),(0.707106781186548-0.7071067811865478j),1.0000000000000002

1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0



lowpass fir equiripple
k = -0.0015512937125081645

1.9596225209927416,(1.429456576611562+0.5256136398354366j),(1.4294565766115626-0.5256136398354371j),(0.2970307305068789+0.9548679202562748j),(0.2060427153245215+0.9785429982692148j),(0.05242470224067809+0.9986248798197328j),(-0.13557360216525924+0.9907672776166596j),(-0.335457123372909+0.9420554752127769j),(-0.529427847409524+0.848354969565993j),(-0.7025006803061771+0.7116830714365494j),(-0.8432392818086838+0.5375383833874265j),(-0.9423478092783396+0.3346350345500443j),(-0.9935295015362752+0.1135743350281364j),(-0.9935295015362747-0.11357433502813782j),(-0.9423478092783392-0.3346350345500463j),(-0.8432392818086895-0.537538383387428j),(-0.702500680306174-0.7116830714365477j),(-0.5294278474095275-0.8483549695659971j),(-0.3354571233729066-0.9420554752127732j),(-0.13557360216526354-0.9907672776166646j),(0.052424702240680236-0.9986248798197316j),(0.2970307305068772-0.9548679202562744j),(0.20604271532452006-0.9785429982692162j),(0.6162471280315581+0.2265951280385213j),(0.6162471280315587-0.2265951280385215j),0.5103023614432647

0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0

lowpass iir butterworth
k = 1.5605470495655823e-06

-1.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0

0.453916547759992,(0.46133503407084214+0.11367002565095832j),(0.46133503407084214-0.11367002565095832j),(0.48444591983306573+0.22905860866642216j),(0.48444591983306573-0.22905860866642216j),(0.5259832762442596+0.3476500786613604j),(0.5259832762442596-0.3476500786613604j),(0.5910898772536364+0.470231517461967j),(0.5910898772536364-0.470231517461967j),(0.6882568121702188+0.5957991247553514j),(0.6882568121702188-0.5957991247553514j)


bandstop iir elliptic
k = 0.05311849370698291

(-0.3864507821149379+0.9223100308479535j),(-0.17435126118290667+0.9846835216067767j),(0.38645119516305143+0.9223098577793963j),(0.17435173198693213+0.984683438244576j),(0.3864511951630517-0.9223098577793917j),(0.1743517319869306-0.9846834382445785j),(-0.3864507821149394-0.9223100308479532j),(-0.17435126118290412-0.9846835216067792j)

(-0.7097365886903071+0.5656317704728073j),(-0.7097365886903062-0.5656317704728071j),(-0.632648678377167+0.2547574186836483j),(-0.6326486783771663-0.25475741868364776j),(0.7097367868523113+0.5656315755431853j),(0.7097367868523116-0.5656315755431849j),(0.632648839744033+0.2547573404242885j),(0.6326488397440329-0.2547573404242884j)
'''


while True:
    print("Saisie du coefficient k")
    try:
        k = float(input("k ? "))
        if k != 0.0:
            break
        else:
            print("k ne peut pas être nul")
    except ValueError:
        pass

while True:
    print("Saisie des zéros z1, z2, z3, ...")
    try:
        enter = input("Entrer les zéros séparés par une virgule : ")
        if enter == "":
            zeros = []
            break
        else:
            zeros = list(map(complex, enter.split(",")))
            break
    except ValueError:
        pass

while True:
    print("Saisie des pôles p1, p2, p3, ...")
    try:
        enter = input("Entrer les pôles séparés par une virgule : ")
        if enter == "":
            poles = []
            break
        else:
            poles = list(map(complex, enter.split(",")))
            break
    except ValueError:
        pass


filtre = FiltreNumerique(fs=1000, k=k, poles=poles, zeros=zeros)

print("""
Pôles et zéros
--------------
""")

print("zéros :", [_arrondi(z) for z in filtre.zeros])
print("pôles :", [_arrondi(p) for p in filtre.poles])


print("""
Pôles et zéros communs
----------------------
""")

print(filtre.poles_zeros_commun())

print("""
Transmittance (fonction de transfert) en z avec pôles et zéros
--------------------------------------------------------------""")
filtre.afficher_transmittance_z_poles_zeros()


print("""
Transmittance en z
------------------""")

filtre.afficher_transmittance_z_puissance_positive()

ordre = filtre.ordre()

if ordre > 0:
    print("\nAutre écriture avec puissances de z négatives :")
    if ordre == 1:
        print("Au numérateur et au dénominateur, on divise par z\n")
    else:
        print("Au numérateur et au dénominateur, on divise par z{}\n"
              .format(_sup(ordre)))
    filtre.afficher_transmittance_z()

print("""
Passage à la transformée en z
-----------------------------
""")

print("\nH(z) = Y(z)/X(z)\n")

filtre.afficher_transformee_en_z()

if not filtre.coeffs_normalises():
    filtre.afficher_transformee_en_z_normalisee()

print("""
Equation de récurrence
----------------------
""")
filtre.afficher_equation_recurrence()

if not filtre.coeffs_normalises():
    print("\nAutre écriture de l'équation de récurrence :")
    filtre.afficher_equation_recurrence_normalisee()


print("""
Type de filtre
--------------
""")
if filtre.filtre_recursif():
    print("Filtre récursif (réponse impulsionnelle infinie)")
else:
    print("Filtre non récursif (réponse impulsionnelle finie)")


print("""
Ordre du filtre
---------------
{}""".format(ordre))

filtre.afficher_bilan_stabilite()

# réponse impulsionnelle
choix = input("\nRéponse impulsionnelle (y/n) ? ")
if choix != "n":
    filtre.tracer_reponse_impulsionnelle(k=1, ndebut=-2, nfin=50)

# réponse indicielle
choix = input("\nRéponse indicielle (y/n) ? ")
if choix != "n":
    filtre.tracer_reponse_indicielle(k=1, ndebut=-2, nfin=50)

# réponse à une rampe
choix = input("\nRéponse à une rampe (y/n) ? ")
if choix != "n":
    filtre.tracer_reponse_rampe(k=1, ndebut=-2, nfin=50)

# réponse à un sinus
choix = input("\nRéponse à un sinus (y/n) ? ")
if choix != "n":
    f = float(input("Fréquence du sinus (en Hz) ? "))
    fs = float(input("Fréquence d'échantillonnage (en Hz) ? "))
    filtre.fs = fs
    filtre.tracer_reponse_sinus(f=f, k=1, ndebut=-2, nfin=50)

# réponse personnalisée
choix = input("\nRéponse personnalisée (y/n) ? ")
if choix != "n":
    print("Saisie de la séquence d'entrée x(0), x(1), x(2), ...")
    sequence = list(map(float, input("Entrer les valeurs séparées par une \
virgule : ").split(",")))
    filtre.tracer_reponse_personnalisee(sequence, ndebut=-2)

if filtre.stable():
    # inutile de connaître la réponse en fréquence si le filtre est instable
    choix = input("\nRéponse en fréquence (y/n) ? ")
    if choix != "n":
        fs = float(input("Fréquence d'échantillonnage (en Hz) ? "))
        filtre.fs = fs
        filtre.tracer_reponse_en_frequence()
        filtre.tracer_reponse_en_frequence(magnitude_unit='dB')

# diagramme des pôles et zéros
choix = input("\nDiagramme des pôles et zéros (y/n) ? ")
if choix != "n":
    filtre.tracer_diagramme_poles_zeros()
