# -*- coding: utf8 -*-
# python 3
# (C) Fabrice Sincère

from digital_filter_tools import *
from digital_filter_tools import _sup  # affichage sous forme d'exposant
from digital_filter_tools import _arrondi

print('''
Exemple d'utilisation du module digital_filter_tools
''')

__version__ = (0, 4, 1)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"


print("""Equation de récurrence (filtre à moyenne glissante)
Version non récursive :
y(n) = (x(n) + x(n-1) + ...  + x(n-N+1))/N

Version récursive :
y(n) = (x(n) - x(n-N))/N + y(n-1)
""")

N = int(input("Nombre de points ? "))

choix = input("\nVersion non récursive (y/n) ? ")
if choix != "n":
    # coefficients
    b = [1/N]*N
    a = [1.0]
else:
    a = [1.0, -1.0]
    b = [1/N] + [0]*(N-1) + [-1/N]

filtre = FiltreNumerique(fs=1000, a=a, b=b)

print("""
Equation de récurrence
----------------------
""")
filtre.afficher_equation_recurrence()

if not filtre.coeffs_normalises():
    print("\nAutre écriture de l'équation de récurrence :")
    filtre.afficher_equation_recurrence_normalisee()

ordre = filtre.ordre()
print("""
Ordre du filtre
---------------
{}""".format(ordre))

print("""
Type de filtre
--------------
""")
if filtre.filtre_recursif():
    print("Filtre récursif (réponse impulsionnelle infinie)")
else:
    print("Filtre non récursif (réponse impulsionnelle finie)")


print("""
Passage à la transformée en z
-----------------------------
""")

filtre.afficher_transformee_en_z()

if not filtre.coeffs_normalises():
    filtre.afficher_transformee_en_z_normalisee()

print("""
Transmittance (fonction de transfert) en z
------------------------------------------""")

print("\nH(z) = Y(z)/X(z)\n")

filtre.afficher_transmittance_z()

ordre = filtre.ordre()
if ordre > 0:
    print("\nAutre écriture avec puissances de z positives :")
    if ordre == 1:
        print("Au numérateur et au dénominateur, on multiplie par z\n")
    else:
        print("Au numérateur et au dénominateur, on multiplie par z{}\n"
              .format(_sup(ordre)))
    filtre.afficher_transmittance_z_puissance_positive()

print("""
Pôles et zéros
--------------
""")

print("zéros :", [_arrondi(z) for z in filtre.zeros])
print("pôles :", [_arrondi(p) for p in filtre.poles])

print("""
Pôles et zéros communs
----------------------
""")

print(filtre.poles_zeros_commun())


print("""
Transmittance en z avec pôles et zéros
--------------------------------------""")
filtre.afficher_transmittance_z_poles_zeros()

filtre.afficher_bilan_stabilite()

# réponse impulsionnelle
choix = input("\nRéponse impulsionnelle (y/n) ? ")
if choix != "n":
    filtre.tracer_reponse_impulsionnelle(k=1, ndebut=-2, nfin=max(20, N+5))

# réponse indicielle
choix = input("\nRéponse indicielle (y/n) ? ")
if choix != "n":
    filtre.tracer_reponse_indicielle(k=1, ndebut=-2, nfin=max(20, N+5))

# réponse à une rampe
choix = input("\nRéponse à une rampe (y/n) ? ")
if choix != "n":
    filtre.tracer_reponse_rampe(k=1, ndebut=-2, nfin=max(20, 2*N))

# réponse à un sinus
choix = input("\nRéponse à un sinus (y/n) ? ")
if choix != "n":
    f = float(input("Fréquence du sinus (en Hz) ? "))
    fs = float(input("Fréquence d'échantillonnage (en Hz) ? "))
    filtre.fs = fs
    filtre.tracer_reponse_sinus(f=f, k=1, ndebut=-2, nfin=50)

# réponse personnalisée
choix = input("\nRéponse personnalisée (y/n) ? ")
if choix != "n":
    print("Saisie de la séquence d'entrée x(0), x(1), x(2), ...")
    sequence = list(map(float, input("Entrer les valeurs séparées par une \
virgule : ").split(",")))
    filtre.tracer_reponse_personnalisee(sequence, ndebut=-2)

if filtre.stable():
    # inutile de connaître la réponse en fréquence si le filtre est instable
    choix = input("\nRéponse en fréquence (y/n) ? ")
    if choix != "n":
        fs = float(input("Fréquence d'échantillonnage (en Hz) ? "))
        filtre.fs = fs
        filtre.tracer_reponse_en_frequence()
        filtre.tracer_reponse_en_frequence(magnitude_unit='dB')

# diagramme des pôles et zéros
choix = input("\nDiagramme des pôles et zéros (y/n) ? ")
if choix != "n":
    filtre.tracer_diagramme_poles_zeros()
