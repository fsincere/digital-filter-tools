# -*- coding: utf8 -*-
# python 3
# (C) Fabrice Sincère

from digital_filter_tools import *
import matplotlib.pyplot as plt

print('''
Exemple d'utilisation du module digital_filter_tools

Approximation d'un filtre récursif par un filtre non récursif
Méthode :
les N+1 coefficients du filtre non récursif (ordre N) correspondent
aux N+1 premières valeurs de la réponse impulsionnelle du filtre récursif.
''')

__version__ = (0, 0, 3)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"


print("""Equation de récurrence (forme générale)

a0*y(n) = b0*x(n) + b1*x(n-1) + b2*x(n-2) + ...
          -a1*y(n-1) -a2*y(n-2) -a3*y(n-3) +...

(avec a0 non nul et au moins un coefficient b non nul)
""")

while True:
    print("Saisie des coefficients b0, b1, b2, ...")
    try:
        b = list(map(float, input("Entrer les coefficients séparés par une \
virgule : ").split(",")))
        break
    except ValueError:
        pass

while True:
    print("\nSaisie des coefficients a0, a1, a2, ...")
    try:
        a = list(map(float, input("Entrer les coefficients séparés par une \
virgule : ").split(",")))
        if a[0] != 0.0:
            break
        else:
            print("a0 ne peut pas être nul !")
    except ValueError:
        pass

# Exemples (d'après pyFDA)
# lowpass iir elliptic order 5
# b = 0.004691927277691961,0.0012351610712425533,0.005746785676190223,0.005746785676190223,0.0012351610712425525,0.00469192727769196
# a = 1.0,-3.603255898252202,5.637563674261337,-4.6951187911755365,2.0684985810278094,-0.3843398178111593

# bandstop iir elliptic order 8
# b = 0.05311849370698291,-9.389778854451691e-08,0.1742833053320179,-2.4647880066119843e-07,0.24618801160644613,-2.464788006750762e-07,0.1742833053320179,-9.389778854451691e-08,0.0531184937069829
# a = 1.0,-7.190577413496158e-07,-1.0382602127247047,2.0203282580233406e-07,1.1413120092942255,-1.64851127248955e-07,-0.5345385874651425,-1.277005795552455e-08,0.14678435179811977

# bandpass iir chebyshev2 order 10
# b = 0.011663854082211557,-3.469446951953614e-18,-0.026075701685068356,6.938893903907228e-18,0.040087486535265504,-6.938893903907228e-18,-0.04008748653526551,6.938893903907228e-18,0.026075701685068356,3.469446951953614e-18,-0.011663854082211557
# a = 1.0,-5.551115123125783e-16,2.2733761160697266,-1.2212453270876722e-15,2.481005004497109,-1.1102230246251565e-15,1.4428901775454681,-2.914335439641036e-16,0.44873734194303716,-4.163336342344337e-17,0.057821968219860825

print('\nCoefficients b', b)
print('\nCoefficients a', a)

fs = 1000

iir = FiltreNumerique(fs=fs, a=a, b=b)
N1 = iir.ordre()

N2 = int(input('\nOrdre du filtre non recursif ? '))

xn, yn = iir.tracer_reponse_impulsionnelle(k=1, ndebut=-2, nfin=N2+10,
                                           titre="IIR")
f1, gain1, phase1 = iir.tracer_reponse_en_frequence(magnitude_unit='dB')

iir.tracer_diagramme_poles_zeros()

b2 = yn[:N2+1]

fir = FiltreNumerique(fs=fs, a=[1], b=b2)

print("""
Equation de récurrence du filtre non récursif
---------------------------------------------
""")

fir.afficher_equation_recurrence()

print('\nCoefficients b', b2)

fir.tracer_reponse_impulsionnelle(k=1, ndebut=-2, nfin=N2+10, titre="FIR")
f2, gain2, phase2 = fir.tracer_reponse_en_frequence(magnitude_unit='dB')
fir.tracer_diagramme_poles_zeros()

N2 = fir.ordre()

# comparaison des réponses en fréquence
# gain
fig, ax = plt.subplots()
ax.set_xlabel('Frequency [Hz]')
ax.set_ylabel('Gain [dB]')
plt.plot(f1, gain1, color='blue', label='iir N={}'.format(N1), linewidth=2)
plt.plot(f2, gain2, color='red', label='fir approximation N={}'.format(N2),
         linewidth=2)

plt.legend()
plt.grid()
plt.title("Comparaison des gains")
fig.tight_layout()

# phase
fig, ax = plt.subplots()
ax.set_xlabel('Frequency [Hz]')
ax.set_ylabel('Phase [°]')
plt.plot(f1, phase1, color='blue', label='iir N={}'.format(N1), linewidth=2)
plt.plot(f2, phase2, color='red', label='fir approximation N={}'.format(N2),
         linewidth=2)

plt.legend()
plt.grid()
plt.title("Comparaison des phases")
fig.tight_layout()
plt.show()
