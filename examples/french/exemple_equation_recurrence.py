# -*- coding: utf8 -*-
# python 3
# (C) Fabrice Sincère

from digital_filter_tools import *
from digital_filter_tools import _sup  # affichage sous forme d'exposant
from digital_filter_tools import _arrondi

print('''
Exemple d'utilisation du module digital_filter_tools
''')

__version__ = (0, 4, 1)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"


print("""Equation de récurrence (forme générale)

a0*y(n) = b0*x(n) + b1*x(n-1) + b2*x(n-2) + ...
          -a1*y(n-1) -a2*y(n-2) -a3*y(n-3) +...

(avec a0 non nul et au moins un coefficient b non nul)
""")

'''
Exemples :
lowpass fir moving average
0.125,0.125,0.125,0.125,0.125,0.125,0.125,0.125

lowpass iir moving average
0.125,0.0,0.0,0.0,0.0,0.0,0.0,0.0,-0.125

1.0,-1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0


lowpass fir equiripple
-0.0015512937125081645,-0.0020039970587341616,0.0008686964831753557,0.008250387253849559,0.015806770171426912,0.014318681694389754,-0.0030495376089023883,-0.030718636403444657,-0.04734513924160639,-0.026068854926953915,0.04550558602163222,0.15002272978504214,0.24406226022092173,0.28189876762927385,0.24406226022092173,0.15002272978504214,0.04550558602163222,-0.026068854926953915,-0.04734513924160639,-0.030718636403444657,-0.0030495376089023883,0.014318681694389754,0.015806770171426912,0.008250387253849559,0.0008686964831753557,-0.0020039970587341616,-0.0015512937125081645

lowpass iir butterworth (pb précision sur le calcul des zéros (-1) 5 % d'erreur,
même pb avec pyfda)
1.5605470495655823e-06,1.7166017545221404e-05,8.583008772610703e-05,0.0002574902631783211,0.0005149805263566421,0.000720972736899299,0.000720972736899299,0.0005149805263566421,0.00025749026317832107,8.583008772610703e-05,1.7166017545221404e-05,1.5605470495655823e-06

1.0,-5.956138386904038,16.84985474873038,-29.612109874826082,35.71666305019519,-30.919510856583457,19.543556339264114,-8.997907129198374,2.9514004467809714,-0.6558046599486282,0.08872230742719463,-0.005529984579748596


bandstop iir elliptic
0.05311849370698291,-9.389778854451691e-08,0.1742833053320179,-2.4647880066119843e-07,0.24618801160644613,-2.464788006750762e-07,0.1742833053320179,-9.389778854451691e-08,0.0531184937069829

1.0,-7.190577413496158e-07,-1.0382602127247047,2.0203282580233406e-07,1.1413120092942255,-1.64851127248955e-07,-0.5345385874651425,-1.277005795552455e-08,0.14678435179811977
'''


while True:
    print("Saisie des coefficients b0, b1, b2, ...")
    try:
        b = list(map(float, input("Entrer les coefficients séparés par une \
virgule : ").split(",")))
        break
    except ValueError:
        pass

while True:
    print("\nSaisie des coefficients a0, a1, a2, ...")
    try:
        a = list(map(float, input("Entrer les coefficients séparés par une \
virgule : ").split(",")))
        if a[0] != 0.0:
            break
        else:
            print("a0 ne peut pas être nul !")
    except ValueError:
        pass

filtre = FiltreNumerique(fs=1000, a=a, b=b)

print("""
Equation de récurrence
----------------------
""")
filtre.afficher_equation_recurrence()

if not filtre.coeffs_normalises():
    print("\nAutre écriture de l'équation de récurrence :")
    filtre.afficher_equation_recurrence_normalisee()

ordre = filtre.ordre()
print("""
Ordre du filtre
---------------
{}""".format(ordre))

print("""
Type de filtre
--------------
""")
if filtre.filtre_recursif():
    print("Filtre récursif (réponse impulsionnelle infinie)")
else:
    print("Filtre non récursif (réponse impulsionnelle finie)")

print("""
Passage à la transformée en z
-----------------------------
""")

filtre.afficher_transformee_en_z()

if not filtre.coeffs_normalises():
    filtre.afficher_transformee_en_z_normalisee()

print("""
Transmittance (fonction de transfert) en z
------------------------------------------""")

print("\nH(z) = Y(z)/X(z)\n")

filtre.afficher_transmittance_z()


if ordre > 0:
    print("\nAutre écriture avec puissances de z positives :")
    if ordre == 1:
        print("Au numérateur et au dénominateur, on multiplie par z\n")
    else:
        print("Au numérateur et au dénominateur, on multiplie par z{}\n"
              .format(_sup(ordre)))
    filtre.afficher_transmittance_z_puissance_positive()

print("""
Pôles et zéros
--------------
""")

print("zéros :", [_arrondi(z) for z in filtre.zeros])
print("pôles :", [_arrondi(p) for p in filtre.poles])


print("""
Pôles et zéros communs
----------------------
""")

print(filtre.poles_zeros_commun())


print("""
Transmittance en z avec pôles et zéros
--------------------------------------""")
filtre.afficher_transmittance_z_poles_zeros()

filtre.afficher_bilan_stabilite()

# réponse impulsionnelle
choix = input("\nRéponse impulsionnelle (y/n) ? ")
if choix != "n":
    filtre.tracer_reponse_impulsionnelle(k=1, ndebut=-2, nfin=50)

# réponse indicielle
choix = input("\nRéponse indicielle (y/n) ? ")
if choix != "n":
    filtre.tracer_reponse_indicielle(k=1, ndebut=-2, nfin=50)

# réponse à une rampe
choix = input("\nRéponse à une rampe (y/n) ? ")
if choix != "n":
    filtre.tracer_reponse_rampe(k=1, ndebut=-2, nfin=50)

# réponse à un sinus
choix = input("\nRéponse à un sinus (y/n) ? ")
if choix != "n":
    f = float(input("Fréquence du sinus (en Hz) ? "))
    fs = float(input("Fréquence d'échantillonnage (en Hz) ? "))
    filtre.fs = fs
    filtre.tracer_reponse_sinus(f=f, k=1, ndebut=-2, nfin=50)

# réponse personnalisée
choix = input("\nRéponse personnalisée (y/n) ? ")
if choix != "n":
    print("Saisie de la séquence d'entrée x(0), x(1), x(2), ...")
    sequence = list(map(float, input("Entrer les valeurs séparées par une \
virgule : ").split(",")))
    filtre.tracer_reponse_personnalisee(sequence, ndebut=-2)

if filtre.stable():
    # inutile de connaître la réponse en fréquence si le filtre est instable
    choix = input("\nRéponse en fréquence (y/n) ? ")
    if choix != "n":
        fs = float(input("Fréquence d'échantillonnage (en Hz) ? "))
        filtre.fs = fs
        filtre.tracer_reponse_en_frequence()
        filtre.tracer_reponse_en_frequence(magnitude_unit='dB')

# diagramme des pôles et zéros
choix = input("\nDiagramme des pôles et zéros (y/n) ? ")
if choix != "n":
    filtre.tracer_diagramme_poles_zeros()
